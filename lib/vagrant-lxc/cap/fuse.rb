module VagrantPlugins
  module GuestDebian
    module Cap
      # makes sure the fuse device exists on the guest so
      # things like sshfs will work
      class FuseDevice
        def self.fuse_device_create(machine)
          # https://jtrancas.wordpress.com/2011/02/09/fuse-filesystems-lxc-container/
          machine.communicate.sudo("mknod /dev/fuse c 10 229")
        end

        def self.fuse_device_present(machine)
          machine.communicate.test("stat /dev/fuse")
        end
      end
    end
  end
end
