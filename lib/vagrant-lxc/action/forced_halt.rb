module Vagrant
  module LXC
    module Action
      class ForcedHalt
        def initialize(app, env)
          @app = app
        end

        def call(env)

          if env[:machine].provider.state.id == :running

            if env.fetch(:force_halt, false)
              env[:ui].info I18n.t("vagrant_lxc.messages.force_shutdown")
              env[:machine].provider.driver.forced_halt
            else
              env[:ui].info I18n.t("vagrant_lxc.messages.shutdown")
              begin
                Timeout.timeout(env[:machine].config.vm.graceful_halt_timeout) do
                  env[:machine].provider.driver.halt
                end
              rescue Timeout::Error
                env[:ui].info I18n.t("vagrant_lxc.messages.shutdown_timeout")
                env[:machine].provider.driver.forced_halt
              end
            end
          end

          @app.call(env)
        end
      end
    end
  end
end
