module Vagrant
  module LXC
    module Action
      class CreateFuseDevice

        def initialize(app, env)
          @app = app
          @env = env
        end

        def call(env)

          machine = env[:machine]

          if !machine.guest.capability?(:fuse_device_create)
            machine.ui.warn(I18n.t("vagrant_lxc.messages.fuse_device_missing"))
          else
            if !machine.guest.capability(:fuse_device_present)
              machine.ui.info(I18n.t("vagrant_lxc.messages.fuse_device_create"))
              machine.guest.capability(:fuse_device_create)
            end
          end

          @app.call(env)
        end

      end
    end
  end
end
